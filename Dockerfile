FROM maven:3.5.0-jdk-7 as builder
ADD . /tmp/my-app/
WORKDIR /tmp/my-app/
RUN mvn package

FROM tomcat:alpine as webcontainer
COPY --from=builder /tmp/my-app/target/my-app.war /usr/local/tomcat/webapps/my-app.war
COPY index.jsp /usr/local/tomcat/webapps/ROOT/
